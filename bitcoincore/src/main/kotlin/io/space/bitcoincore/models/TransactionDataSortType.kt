package io.space.bitcoincore.models

enum class TransactionDataSortType {
    None, Shuffle, Bip69
}
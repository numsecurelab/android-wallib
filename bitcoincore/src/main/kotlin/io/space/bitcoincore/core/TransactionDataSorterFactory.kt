package io.space.bitcoincore.core

import io.space.bitcoincore.models.TransactionDataSortType
import io.space.bitcoincore.utils.Bip69Sorter
import io.space.bitcoincore.utils.ShuffleSorter
import io.space.bitcoincore.utils.StraightSorter

class TransactionDataSorterFactory : ITransactionDataSorterFactory {
    override fun sorter(type: TransactionDataSortType): ITransactionDataSorter {
        return when (type) {
            TransactionDataSortType.None -> StraightSorter()
            TransactionDataSortType.Shuffle -> ShuffleSorter()
            TransactionDataSortType.Bip69 -> Bip69Sorter()
        }
    }
}
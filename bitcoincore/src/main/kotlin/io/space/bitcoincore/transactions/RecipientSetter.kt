package io.space.bitcoincore.transactions

import io.space.bitcoincore.core.IPluginData
import io.space.bitcoincore.core.IRecipientSetter
import io.space.bitcoincore.core.PluginManager
import io.space.bitcoincore.transactions.builder.MutableTransaction
import io.space.bitcoincore.utils.IAddressConverter

class RecipientSetter(
        private val addressConverter: IAddressConverter,
        private val pluginManager: PluginManager
) : IRecipientSetter {

    override fun setRecipient(mutableTransaction: MutableTransaction, toAddress: String, value: Long, pluginData: Map<Byte, IPluginData>, skipChecking: Boolean) {
        mutableTransaction.recipientAddress = addressConverter.convert(toAddress)
        mutableTransaction.recipientValue = value

        pluginManager.processOutputs(mutableTransaction, pluginData, skipChecking)
    }

}